use std::io::{self, BufRead};
use std::fs::File;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_csv_file() {
        let file_name = "data.csv";
        let all_lines = csv_reader(file_name).unwrap();
        
        // Test
        assert_eq!(all_lines[1], "0,Kevin,10");
        assert_eq!(all_lines[3], "2,Andrew,30");
    }
}

// CSV reader helper function
fn csv_reader(file_name: &str) -> io::Result<Vec<String>> {
    let file = File::open(file_name)?;
    let reader = io::BufReader::new(file);
    let mut lines = Vec::new();

    for line in reader.lines() {
        lines.push(line?);
    }

    Ok(lines)
}
