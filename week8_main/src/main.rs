use std::fs::File;
use csv::Reader;
use std::env;
use std::error::Error;

// Main function
fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    
    if args.len() < 2 {
        eprintln!("Missing csv path");
        return Ok(());
    }

    let file_name = &args[1];
    let file = File::open(file_name)?;
    let mut reader = Reader::from_reader(file);
    
    // Console-level check
    for result in reader.records() {
        let record = result?;
        println!("{:?}", record);
    }
    
    println!("Finishing reading csv!");
    
    Ok(())
}
